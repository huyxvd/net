﻿### Bạn đang lập trình một trò chơi thu thập quặng và bán chúng để nhận được đồng xu vàng. Quy tắc:
• Với 10 mảnh quặng đầu tiên, người chơi nhận được 10 đồng xu vàng cho mỗi mảnh.
• Với 5 mảnh quặng tiếp theo, người chơi nhận được 5 đồng xu vàng cho mỗi mảnh.
• Với 3 mảnh quặng tiếp theo, người chơi nhận được 2 đồng xu vàng cho mỗi mảnh.
• Với tất cả các mảnh quặng còn lại, người chơi nhận được 1 đồng xu vàng cho mỗi mảnh.
Ví dụ: 25 mảnh quặng => nhận được 10 * 10 + 5 * 5 + 3 * 2 + 7 * 1 = 138 đồng xu vàng.
11 mảnh quặng => nhận được 10 * 10 + 1 * 5 = 105 đồng xu vàng.
Hãy viết một chương trình để thực hiện tính toán số xu vàng nhận được với yêu cầu:
1. Yêu cầu người chơi nhập số mảnh quặng đã thu thập.
2. In ra số lượng đồng xu vàng mà người chơi nhận được cho quặng đó.