﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.Write("Nhập số lượng quặng người chơi đã thu thập: ");
int soLuongQuang = int.Parse(Console.ReadLine());

int quang10Dau = Math.Min(soLuongQuang, 10);
int vangCua10QuangDau = quang10Dau * 10;
soLuongQuang -= quang10Dau;
Console.WriteLine("Vàng từ 10 quặng đầu tiên bạn nhận được: " + vangCua10QuangDau);

int quang5TiepTheo = Math.Min(soLuongQuang, 5);
int vangCua5QuangTiepTheo = quang5TiepTheo * 5;
soLuongQuang -= quang5TiepTheo;
Console.WriteLine("Vàng từ 5 quặng tiếp theo bạn nhận được: " + vangCua5QuangTiepTheo);

int quang3TiepTheo = Math.Min(soLuongQuang, 3);
int vangCua3QuangTiepTheo = quang3TiepTheo * 2;
soLuongQuang -= quang3TiepTheo;
Console.WriteLine("Vàng từ 3 quặng tiếp theo bạn nhận được: " + vangCua3QuangTiepTheo);

int vangConLai = soLuongQuang * 1;
Console.WriteLine("Vàng còn lại bạn nhận được: " + vangConLai);

int tongVang = vangCua10QuangDau + vangCua5QuangTiepTheo + vangCua3QuangTiepTheo + vangConLai;
Console.WriteLine("Tổng số vàng bạn nhận được là: " + tongVang);