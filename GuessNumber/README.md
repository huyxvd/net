﻿## Trò chơi đoán số

### Bạn sẽ viết một chương trình cho một trò chơi đoán số. Trò chơi này hoạt động như sau:

Chương trình sẽ chọn một số ngẫu nhiên trong khoảng từ 0 đến 99.
gợi ý random số: int number = Random.Shared.Next(0, 100);
Người chơi cần đoán số mà chương trình đã chọn.
Nếu người chơi đoán đúng số, chương trình sẽ in ra thông báo "Chúc mừng! Bạn đã đoán đúng số." và kết thúc trò chơi.
Nếu người chơi đoán sai số, chương trình sẽ in ra thông báo "Số bạn đoán là quá lớn." hoặc "Số bạn đoán là quá nhỏ." để chỉ dẫn người chơi tiếp tục đoán.
Trò chơi sẽ tiếp tục cho đến khi người chơi đoán đúng số.

Gợi ý: Sử dụng vòng lặp để cho phép người chơi đoán liên tục cho đến khi đoán đúng.
Sử dụng cấu trúc rẽ nhánh if-else để kiểm tra số đoán của người chơi.
