﻿Random ngauNhien = new Random();
int soCanDoan = ngauNhien.Next(0, 100);
bool daDoanDung = false;

Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.WriteLine("Chào mừng bạn đến với Trò chơi đoán số!");
Console.WriteLine("Hãy thử đoán số nằm trong khoảng từ 0 đến 99.");

while (!daDoanDung)
{
    Console.Write("Nhập số bạn đoán: ");
    int soDoan = int.Parse(Console.ReadLine());

    if (soDoan == soCanDoan)
    {
        Console.WriteLine("Chúc mừng! Bạn đã đoán đúng số.");
        daDoanDung = true;
    }
    else if (soDoan < soCanDoan)
    {
        Console.WriteLine("Số bạn đoán quá nhỏ. Hãy thử lại.");
    }
    else
    {
        Console.WriteLine("Số bạn đoán quá lớn. Hãy thử lại.");
    }
}

Console.WriteLine("Cảm ơn bạn đã tham gia trò chơi!");