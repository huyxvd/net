﻿## Yêu Cầu Tựa Đề Trò Chơi Tic-Tac-Toe

Trong trò chơi này, bạn sẽ viết một ứng dụng console để thực hiện trò chơi Tic-Tac-Toe giữa hai người chơi. Trò chơi sẽ cho phép họ thay phiên nhau điền kí hiệu "X" và "O" vào bảng Tic-Tac-Toe. Ứng dụng cũng sẽ kiểm tra và thông báo kết quả khi một người chơi thắng hoặc khi trận hòa.

### Mô tả bài toán:

Tạo bảng Tic-Tac-Toe có kích thước 3x3 để thể hiện trạng thái của trò chơi.
Hai người chơi sẽ thay phiên nhau nhập vị trí mà họ muốn điền kí hiệu của họ ("X" hoặc "O") vào bảng.
Ứng dụng sẽ kiểm tra sau mỗi lượt đánh để xem liệu có người chơi nào đã thắng dựa trên các hàng ngang, cột dọc và đường chéo.
Nếu trận đấu kết thúc mà không có người thắng, ứng dụng sẽ thông báo trận hòa.