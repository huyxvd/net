﻿char[] board = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' };
char currentPlayer = 'X';

void ResetBoard()
{
    for (int i = 0; i < board.Length; i++)
    {
        board[i] = ' ';
    }
}

void DrawBoard()
{
    Console.WriteLine($" {board[0]} | {board[1]} | {board[2]} ");
    Console.WriteLine("---+---+---");
    Console.WriteLine($" {board[3]} | {board[4]} | {board[5]} ");
    Console.WriteLine("---+---+---");
    Console.WriteLine($" {board[6]} | {board[7]} | {board[8]} ");
}

int GetPlayerMove()
{
    Console.WriteLine($"Lượt của người chơi {currentPlayer}. Nhập nước đi (1-9): ");
    return int.Parse(Console.ReadLine()) - 1;
}

bool IsValidMove(int move)
{
    return (move >= 0 && move < 9 && board[move] == ' ');
}

void MakeMove(int move)
{
    board[move] = currentPlayer;
}

bool CheckWin()
{
    return (CheckLine(0, 1, 2) || CheckLine(3, 4, 5) || CheckLine(6, 7, 8) ||
            CheckLine(0, 3, 6) || CheckLine(1, 4, 7) || CheckLine(2, 5, 8) ||
            CheckLine(0, 4, 8) || CheckLine(2, 4, 6));
}

bool CheckLine(int a, int b, int c)
{
    return (board[a] != ' ' && board[a] == board[b] && board[b] == board[c]);
}

bool IsBoardFull()
{
    foreach (char c in board)
    {
        if (c == ' ')
        {
            return false;
        }
    }
    return true;
}

void SwitchPlayer()
{
    currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
}

Console.WriteLine("Chào mừng đến với trò chơi Tic-Tac-Toe!");

while (true)
{
    Console.OutputEncoding = System.Text.Encoding.UTF8;
    ResetBoard();

    while (true)
    {
        Console.Clear();
        DrawBoard();
        int move = GetPlayerMove();

        if (IsValidMove(move))
        {
            MakeMove(move);
            if (CheckWin())
            {
                Console.Clear();
                DrawBoard();
                Console.WriteLine($"Người chơi {currentPlayer} thắng!");
                break;
            }
            else if (IsBoardFull())
            {
                Console.Clear();
                DrawBoard();
                Console.WriteLine("Trận hòa!");
                break;
            }
            SwitchPlayer();
        }
        else
        {
            Console.WriteLine("Nước đi không hợp lệ. Nhấn phím bất kỳ để tiếp tục.");
            Console.ReadKey();
        }
    }

    Console.WriteLine("Bạn có muốn chơi lại? (y/n)");
    if (Console.ReadLine().ToLower() != "y")
    {
        break;
    }
}

Console.WriteLine("Cảm ơn bạn đã chơi trò chơi Tic-Tac-Toe!");
